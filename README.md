# OpenML dataset: Russian-Presidential-Elections-2018

https://www.openml.org/d/43661

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
All the time of russian elections history we have some insteresting anomalies in the voting results. You can use this dataset to find them)
Content
So, the each row of the dataset is detailed voting results from local election commission - a lowest level of the Russian election system. Local election sites are providing a way to vote for a small group of people living nearby. Often, the number of linked voters are between 1000-2000 for each election site. But for the foreign based election sites(on the Russian embassies over the world) the number of linked voters can be up to 7000. 
Acknowledgements
Github version is here: https://github.com/Rexhaif/RussianPresidentialElection2018. 
The raw data are taken from Sergey Shpilkin, famous Russian election statistics researcher. Here is his blog: https://podmoskovnik.livejournal.com/ . You can download the raw data from google drive: https://drive.google.com/file/d/1hQQM_ceSzwVWBVyJJynW3_3UNKaJe4o5/view?usp=sharing .

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43661) of an [OpenML dataset](https://www.openml.org/d/43661). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43661/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43661/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43661/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

